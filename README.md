# Sales Forecast Model

This is a ARIMA (autoregressive moving average) and Time series based Sales Forecast model that uses pre-existing data and calculations to make future projections. Various external factors such as Seasonality and Volatility etc. will be considered.

This project contains:
- Excel sample data (containing formulaic representations)
- Data obtained from Amazon API
- Sales_Forecast.py python based model